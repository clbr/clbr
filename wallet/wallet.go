package wallet

import "log"

// Init fires up the cli wallet
func Init(cfgDir string) {
	log.Print("Starting Calibrae Wallet")
	log.Printf("Using work directory: %s\n", cfgDir)
}
