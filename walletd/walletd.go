package walletd

import "log"

// Init fires up the gRPC wallet
func Init(cfgDir string) {
	log.Print("Starting Calibrae Wallet daemon")
	log.Printf("Using work directory: %s\n", cfgDir)
}
