package main

import (
	"os"

	"gitlab.com/clbr/clbr/server"
	"gitlab.com/clbr/clbr/wallet"
	"gitlab.com/clbr/clbr/walletd"
	"gitlab.com/clbr/flag"
	"gitlab.com/clbr/proc"
	"gitlab.com/clbr/subcmd"
)

// wallet subcommand
type wal struct {
	cfgDir string
}

func (*wal) Name() string     { return "wallet" }
func (*wal) Synopsis() string { return "Runs Calibrae CLI wallet" }
func (*wal) Usage() string {
	return `Calibrae wallet usage:
`
}
func (parent *wal) WriteFlags(flags *flag.Set) {
	flags.StringVar(&parent.cfgDir, "d", "~/.calibrae", "data directory")
}
func (parent *wal) Execute(
	_ proc.Context, flags *flag.Set, _ ...interface{}) subcmd.ExitStatus {

	wallet.Init(parent.cfgDir)
	return subcmd.ExitSuccess
}

// walletd subcommand
type wald struct {
	cfgDir    string
	debugFlag bool
}

func (*wald) Name() string     { return "walletd" }
func (*wald) Synopsis() string { return "Runs Calibrae wallet daemon" }
func (*wald) Usage() string {
	return `Calibrae wallet daemon usage:
`
}
func (parent *wald) WriteFlags(flags *flag.Set) {
	flags.StringVar(&parent.cfgDir, "d", "wallet.d", "Data directory")
}
func (parent *wald) Execute(
	_ proc.Context, flags *flag.Set, _ ...interface{}) subcmd.ExitStatus {

	walletd.Init(parent.cfgDir)
	return subcmd.ExitSuccess
}

// server subcommand
type srv struct {
	cfgDir    string
	mode      string
	debugFlag bool
}

func (*srv) Name() string     { return "server" }
func (*srv) Synopsis() string { return "Runs Calibrae server daemon" }
func (*srv) Usage() string {
	return `Calibrae server daemon usage:
`
}
func (parent *srv) WriteFlags(flags *flag.Set) {
	flags.StringVar(&parent.cfgDir, "d", "calibr.d", "data directory")
	flags.StringVar(&parent.mode, "m", "rpc", "mode: router|cache|witness|ledger|mirror|rpc")
	flags.BoolVar(&parent.debugFlag, "debug", false, "Enables more verbose logging")
}
func (parent *srv) Execute(
	_ proc.Context, flags *flag.Set, _ ...interface{}) subcmd.ExitStatus {

	server.Init(parent.cfgDir, parent.mode, parent.debugFlag)
	return subcmd.ExitSuccess
}

// Key management subcommand
type keys struct {
}

func main() {
	subcmd.Register(&wal{}, "interface")
	subcmd.Register(&wald{}, "interface")
	subcmd.Register(&srv{}, "servers")
	subcmd.Register(subcmd.Help(), "subcommands")
	subcmd.Register(subcmd.Flags(), "subcommands")
	subcmd.Register(subcmd.Commands(), "subcommands")

	flag.Parse()
	ctx := proc.Background()
	os.Exit(int(subcmd.Execute(ctx)))
}
