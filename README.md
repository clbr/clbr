# Calibrae

Based on Steem and Sporedb, but written completely from scratch, including all dependencies re-implemented precisely to requirements.

## Code is written for Humans, first.

**The code should be as easy to read as good documentation.**

If it is hard for a human to read, then the logic errors it may contain may remain invisible.

If it is not easy to read, then it must be changed. For this reason, this project is a total rewrite, refactor, and rethink of the things it is built from.

Names need to make sense, names need to not stutter, and things should be grouped logically so that everything follows, like the coloured lines that help you find your way around the Black Mesa facility.

## Wiki

See the wiki page at [https://gitlab.com/clbr/clbr/wikis/Calibrae](https://gitlab.com/clbr/clbr/wikis/Calibrae) for more detailed information.