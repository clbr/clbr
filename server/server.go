package server

import (
	"log"
	"os"
)

var (
	logInfo   *log.Logger
	debugMode bool
	execMode  string
)

func Init(cfgDir string, mode string, debugFlag bool) {
	debugMode = debugFlag
	logInit(debugFlag)
	logInfo.Print("Starting Calibrae Server")
	switch mode {
	case "router", "cache", "witness", "ledger", "rpc":
	default:
		logInfo.Print("Valid options are router|cache|witness|ledger|mirror|rpc")
		logInfo.Fatal("Invalid server mode '" + mode + "'")
	}
	execMode = mode
	logInfo.Printf("Running in '%s' mode\n", mode)
	_, err := os.Stat(cfgDir)
	if os.IsNotExist(err) {
		err = os.Mkdir(cfgDir, 0711)
		if err == nil {
			logInfo.Printf("Created work directory '%s'\n", cfgDir)
		} else {
			logInfo.Fatal(err)
		}
	}
	logInfo.Printf("Using work directory: %s\n", cfgDir)
}

func debugPrint(debugInfo string) {
	if debugMode {
		logInfo.Println(debugInfo)
	}
}

func logInit(debugFlag bool) {
	if debugFlag {
		logInfo = log.New(os.Stdout,
			"",
			log.Ldate|log.Lmicroseconds|log.Llongfile|log.LUTC)
	} else {
		logInfo = log.New(os.Stdout,
			"",
			log.LUTC|log.Ldate|log.Ltime|log.Lshortfile)
	}
}
